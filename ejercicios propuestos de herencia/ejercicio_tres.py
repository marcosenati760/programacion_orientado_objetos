from array import array

numeros = array('d', [10.0, 3.14, 2.71828, 127.0])
numeros.insert(0, 10)
print(numeros) # Imprime array('d', [10.0, 10.0, 3.14, 2.71828, 127.0])
print(numeros[1]) # Imprime 10
print(numeros[2]) # Imprime 3.
import math

class Math2(math):
    @staticmethod
    def maximo(array):
        return max(array)

    @staticmethod
    def minimo(array):
        return min(array)

    @staticmethod
    def sumatoria(array):
        return sum(array)

    @staticmethod
    def media_aritmetica(array):
        return sum(array) / len(array)

    @staticmethod
    def media_geometrica(array):
        producto = 1
        for num in array:
            producto *= num
        return producto ** (1/len(array))

        math2 = Math2()
array = [1, 2, 3, 4, 5]
print(math2.maximo(array))
print(math2.minimo(array))
print(math2.sumatoria(array))
print(math2.media_aritmetica(array))
print(math2.media_geometrica(array))
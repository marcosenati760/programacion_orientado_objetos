class Precio:
    def __init__(self, valor):
        self.valor = valor

class Factura(Precio):
    def __init__(self, valor, emisor, cliente):
        super().__init__(valor)
        self.emisor = emisor
        self.cliente = cliente

    def imprimirfactura(self):
        print("Factura")
        print("Valor: ", self.valor)
        print("Emisor: ", self.emisor)
        print("Cliente: ", self.cliente)
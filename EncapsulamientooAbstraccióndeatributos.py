class Persona:  
	def __init__(self, nombre, apellidos, edad):  
		self._nombre= nombre
		self.apellidos = apellidos 
		self.__edad=  edad
	
	def camina(self):
		print(self.nombre + " está caminando")
        
	@property
 	def nombre(self):
		return self._nombre
    
    @property
    def edad(self):
        return self.__edad

p1 = Persona("Mike", "Mendiola", 25)
p1.camina()
print(p1.nombre)  
print(p1.edad)
